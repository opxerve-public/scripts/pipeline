import subprocess
import sys

def push_to_branch(branch, git_remote, commit_msg):
    subprocess.run(['git', 'status'])
    subprocess.run(['git', 'add', '.'])
    print("Added all changes")
    process = subprocess.run(['git', 'commit', '-m', commit_msg], capture_output=True, text=True)
    print("Committed")
    if process.returncode and "nothing to commit" in process.stdout:
        print("Nothing change")
        exit(0)
    elif not process.returncode:
        print("Push update")
        subprocess.run(['git', 'remote', 'set-url', 'origin', git_remote])
        subprocess.run(['git', 'push', 'origin', branch])
    else:
        print("Got an error: {}".format(process.stdout))
        exit(1)

if __name__ == '__main__':
    branch = sys.argv[1]
    docker_tag = sys.argv[2]
    service = sys.argv[3]
    git_remote = sys.argv[4]

    push_to_branch(branch, git_remote, 'Update {} version to {}'.format(service, docker_tag))
